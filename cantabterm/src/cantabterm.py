#
# Copyright (C) 2021  Andrew Jenkins
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the “Software”),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#

# Dead simple file that lets you find out what term it is.
# (Ab)uses https://www.cam.ac.uk/about-the-university/term-dates-and-calendars

# options are "michaelmas", "lent", "easter"

import datetime
import json

import requests
from bs4 import BeautifulSoup

TERM_NAMES = ["michaelmas", "lent", "easter"]
URL = "https://www.cam.ac.uk/about-the-university/term-dates-and-calendars"

class Term():
    """
From https://www.cam.ac.uk/about-the-university/term-dates-and-calendars

"The Michaelmas Term shall begin on 1 October and shall consist of
eighty days, ending on 19 December.

The Lent Term shall begin on 5 January and shall consist of eighty days,
ending on 25 March or in any leap year on 24 March.

The Easter Term shall begin on 10 April and shall consist of seventy
days ending on 18 June, provided that in any year in which full
Easter Term begins on or after 22 April the Easter Term shall 
begin on 17 April and end on 25 June."
"""
    def __init__(self, name, full_term_start_date, full_term_end_date):
        self.name = name
        year = full_term_start_date.year
        self.calendar_year = year

        if name == "michaelmas":
            self.start_date = datetime.date(year, 10, 1)
            self.end_date = datetime.date(year, 12, 19)
            self.academic_year = year
        elif name == "lent":
            self.start_date = datetime.date(year, 1, 5)
            self.end_date = datetime.date(year, 3, 24) if is_leap_year(year) else datetime.date(year, 3, 25)
            self.academic_year = year - 1
        elif name == "easter":
            if full_term_start_date >= datetime.date(year, 4, 22):
                # on or after april 22
                self.start_date = datetime.date(year, 4, 17)
                self.end_date = datetime.date(year, 6, 25)
            else:
                self.start_date = datetime.date(year, 4, 10)
                self.end_date = datetime.date(year, 6, 28)

            self.academic_year = year - 1

        else:
            raise ValueError("Only valid term names are 'michaelmas', 'lent' and 'easter'.")

        self.full_term_start_date = full_term_start_date
        self.full_term_end_date = full_term_end_date

    def full_term_week_start_date(self, n):
        """On what date does week `n` of full term start?"""
        return self.full_term_start_date + datetime.timedelta(days=2+7*(n-1))

    def date_in_term(self, d):
        """True if the date is within the term."""
        return self.start_date <= d <= self.end_date

    def date_in_full_term(self, d):
        """True if the date is within full term for this term."""
        return self.full_term_start_date <= d <= self.full_term_end_date

    def date_full_term_week(self, d):
        """Which week of term is date `d`?"""
        # week 1 starts on the first Thursday of full term.
        days = (d - self.full_term_start_date).days - 2 # since thu is two days after tue
        week = 1 + (days // 7)
        return week

    def to_dict(self):
        # handy for making JSON
        return {"name": self.name,
                "academic_year" : self.academic_year,
                "calendar_year" : self.calendar_year,
                "start_date": self.start_date.isoformat(),
                "end_date": self.end_date.isoformat(),
                "full_term_start_date": self.full_term_start_date.isoformat(),
                "full_term_end_date": self.full_term_end_date.isoformat()}

    @classmethod
    def from_dict(cls, d):
        result = cls(d["name"], datetime.date.fromisoformat(d["full_term_start_date"]), datetime.date.fromisoformat(d["full_term_end_date"]))
        if result.to_dict() != d:
            raise ValueError("Couldn't load from dict perfectly.")
        else:
            return result

    def pretty_name(self):

        return "{} {}".format(self.name.title(), self.calendar_year)

    def __repr__(self):
        return "<{}.Term object: {}>".format(__name__, self.pretty_name())

def is_leap_year(yr):
    # A year is a leap year if:
    # It is a century year and divisible by 400 OR
    # It is not a century year and divisible by 4
    if yr < 1000:
        raise ValueError("year < 1000. You've probably done something wrong.")
    else:
        return ((yr % 4 == 0) and not (yr % 100 == 0)) or (yr % 400 == 0)

def format_academic_year(year):
    """i.e. 2019->"2019-20", 1999->"1999-2000" """
    s0 = str(year)
    s1 = str(year+1)
    if s1[:2] == s0[:2]:
        return "{}-{}".format(s0,s1[2:])
    else:
        return "{}-{}".format(s0,s1)

months= ["January", "February", "March", "April", "May", "June", "July", 
"August", "September", "October", "November", "December"]

days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", 
"Saturday", "Sunday"]

def parse_date(s, year):
    # Deal with something like "Tue 4 Apr"
    s = s.strip()
    items = [i.strip() for i in s.split(" ") if i.strip()]
    # items[1] should be a day

    for i, m in enumerate(months):
        if m.startswith(items[2]):
            month_no = i + 1 # Jan is 1, and so on.
            break
    else:
        raise ValueError("Failed to identify month '{}'".format(items[2]))

    date = datetime.date(year, month_no, int(items[1]))

    return date

def parse_daterange(s, year):
    items = [i.strip() for i in s.split("–")] # note that this is an n-dash.
    return parse_date(items[0], year), parse_date(items[1], year)

def _get_soup():
    with open("termdates.html") as f:
        return BeautifulSoup(f.read(), "html.parser")

class TermDB:

    def __init__(self, terms_by_academic_year):
        self.terms_by_academic_year = terms_by_academic_year

    def get_term(self, year, name, year_type="academic"):
        if year_type not in ["academic", "calendar"]:
            raise ValueError("year_type must be 'academic' or 'calendar', not {}".format(year_type))

        if name.lower() in ["hilary", "trinity"]:
            print("#GDBO")
            print("Light blue > dark blue")
            raise ValueError("Nice try, Oxon. Only valid term names are 'michaelmas', 'lent' and 'easter'.")

        if name not in TERM_NAMES:
            raise ValueError("Only valid term names are 'michaelmas', 'lent' and 'easter'.")

        if year_type == "calendar" and name != "michaelmas":
            # because those terms happen in the new year, the calendar year
            # is one greater than the academic year
            year -= 1

        return self.terms_by_academic_year[year][TERM_NAMES.index(name)]

    def term_for_date(self, d, full_term=False):
        """Returns either the Term object which includes that date, or None.
        If full_term is True then it will only consider dates within full term."""
        for termname in TERM_NAMES:
            t = self.get_term(d.year, termname, year_type="calendar")
            if t.date_in_full_term(d): # full term is a subset of term
                return t
            elif not full_term and t.date_in_term(d):
                return t

    def next_term_for_date(self, d, full_term=False):
        """If the date d is in a term, then return that term. Otherwise return
        the next term to start after this date. If full_term is True then
        it will only consider full term dates for purposes of the above."""
        # this is chronological order within a calendar year. If we are 
        # after the end of Michaelmas then we will also need to check the
        # next calendar year.

        for year in (d.year, d.year + 1):
            for termname in ["lent", "easter", "michaelmas"]:
                assert termname in TERM_NAMES
                t = self.get_term(year, termname, year_type="calendar")

                if t.date_in_full_term(d): # full term is a subset of term
                    return t
                elif not full_term and t.date_in_term(d):
                    return t
                elif t.full_term_start_date > d:
                    # since full_term start is after term start this will catch
                    # those cases too
                    return t

    def prev_term_for_date(self, d, full_term=False):
        """If the date d is in a term, then return that term. Otherwise return
        the last term to end before this date. If full_term is True then
        it will only consider full term dates for purposes of the above."""
        # the reverse order now compared to next_term_for_date
        for year in (d.year, d.year - 1):
            for termname in ["michaelmas", "easter", "lent"]:
                assert termname in TERM_NAMES
                t = self.get_term(year, termname, year_type="calendar")

                if t.date_in_full_term(d): # full term is a subset of term
                    return t
                elif not full_term and t.date_in_term(d):
                    return t
                elif t.full_term_start_date < d:
                    # since full_term start is after term start this will catch
                    # those cases too
                    return t

    def to_dict(self):
        return {y: [t.to_dict() for t in term] for y, term in self.terms_by_academic_year.items()}


    def save_json(self, filename):
        with open(filename, "w") as f:
            json_terms = json.dump(self.to_dict(), f)


class TermHeuristic(TermDB):
    """This produces a GUESS of the term dates for a given year based on the following rules:
        1. Michaelmas starts on the first Tuesday AFTER the first day of Academic Term (i.e., if 1 Oct is a Tuesday then term starts on 8 Oct. This seems to be a reliable assumption).
        2. The other terms start on the second Tuesday after the first day of Academic Term. (less reliable)
        3. Where this would predict a start date for Easter term before 21 Apr, Easter Term starts on the third Tuesday after the first day of Academic Term.

    The interface is the same as TeamDB.
    """

    def __init__(self):
        pass

    def get_term(self, year, name, year_type="academic"):
        if year_type not in ["academic", "calendar"]:
            raise ValueError("year_type must be 'academic' or 'calendar', not {}".format(year_type))

        if name.lower() in ["hilary", "trinity"]:
            print("#GDBO")
            print("Light blue > dark blue")
            raise ValueError("Nice try, Oxon. Only valid term names are 'michaelmas', 'lent' and 'easter'.")

        if name not in TERM_NAMES:
            raise ValueError("Only valid term names are 'michaelmas', 'lent' and 'easter'.")

        if year_type == "academic" and name != "michaelmas":
            # we use calendar years for this function
            year += 1

        # Lazy first-guess at the term.
        term0 = Term(name, datetime.date(year, 1, 1), None)
        # Monday = 0
        wd = term0.start_date.weekday()
        to_tue = ((1 - wd) % 7)

        dt = datetime.timedelta(days=to_tue)
        # this is the first tuesday of term
        first_tues = term0.start_date + dt 

        if wd == 1: #name == "michaelmas" and wd == 1:
            # Go to the second Tuesday if this would mean FT starting
            # on the first day of academic term.
            first_tues += datetime.timedelta(days=7)

        if name != "michaelmas":
            # want to start on the second Tuesday (third if first day
            # was a Tuesday) for the other terms.
            first_tues += datetime.timedelta(days=7)

        if name == "easter":
            if first_tues < datetime.date(year, 4, 21):
                # tends to start after 21 Apr
                first_tues += datetime.timedelta(days=7)

        last_fri = first_tues + datetime.timedelta(days=(52 if name == "easter" else 59))
        assert last_fri.weekday()==4 # ought to be a friday.
        return Term(name, first_tues, last_fri)

    def to_dict(self):
        raise NotImplemented

def load_term_data_html(html):

    soup = BeautifulSoup(html, "html.parser")
    # returns a dictionary mapping academic years (i.e. 2019-20 under 2019)
    # to a list of terms [mich, lent, easter]
    tab = soup.find("table")
    headers = [i.text.lower() for i in tab.find("thead").find_all("th")]    
    #print(headers)
    year_idx = headers.index("year")
    mich_idx = headers.index("full michaelmas term")
    lent_idx = headers.index("full lent term")
    east_idx = headers.index("full easter term")

    years = {}

    for row in tab.find("tbody").find_all("tr"):

        if "tbc" in row.text.lower() or "tbd" in row.text.lower():
            print("row tbc, skipping")

        else:
            this_year = []

            content = [i for i in row.find_all(("th", "td"))]
            year = int(content[year_idx].text.split("-")[0].strip())
            mich_dates = parse_daterange(content[mich_idx].text, year)
            this_year.append(Term("michaelmas", *mich_dates))

            # Lent and Easter happen after the new year so we add one
            # to the year
            lent_dates = parse_daterange(content[lent_idx].text, year+1)
            this_year.append(Term("lent", *lent_dates))
            east_dates = parse_daterange(content[east_idx].text, year+1)
            this_year.append(Term("easter", *east_dates))

            years[year] = this_year

    return TermDB(years)


def load_term_data_scrape():
    """You are strongly encouraged to cache this data (e.g. by calling
    db.save_json with the returned data)"""
    r = requests.get(URL)
    r.raise_for_status()

    return load_term_data_html(r.text)


def load_term_data_dict(dic):
    """this dict probably came from JSON"""
    return TermDB({int(y) : [Term.from_dict(d) for d in term] for y,term in dic.items()})


def load_term_data_json(filename):
    with open(filename) as f:
        json_terms = json.load(f)

    return load_term_data_dict(json_terms)

if __name__ == "__main__":

    from sys import argv
    if len(argv) > 1 and argv[1].lower() == "scrape":
        db = load_term_data_scrape()
    else:
        print("The following is best-guess information: use `cantabterm scrape` "\
              "to download the term data from the University website.\n")
        db = TermHeuristic()
    td = datetime.date.today()
    today_term = db.term_for_date(td)

    if today_term:
        if today_term.date_in_full_term(td):
            print("It is currently week {} of Full {} Term, academic year {}".format(today_term.date_full_term_week(td), today_term.name.title(), format_academic_year(today_term.academic_year)))
        else:
            print("It is currently {} term (but not full term), academic year {}".format(today_term.name.title(), format_academic_year(today_term.academic_year)))
    else:
        next_term = db.next_term_for_date(td)
        prev_term = db.prev_term_for_date(td)

        print("Currently between terms.")
        print("Last term was {}, academic year {}".format(prev_term.name.title(), format_academic_year(prev_term.academic_year)))
        print("Next term will be {}, academic year {}".format(next_term.name.title(), format_academic_year(next_term.academic_year)))

