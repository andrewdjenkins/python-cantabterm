cantabterm
==========

simple fun way to find out the current, next and previous
Cambridge term (including Full Term) from Python.

Uses ``requests`` and ``BeautifulSoup`` to scrape the university website,
but makes it very easy to cache the results and you are strongly
encouraged to do so. Caching may even become default at some point,
who knows?
