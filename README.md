# cantabterm

Scrapes the [university website](https://www.cam.ac.uk/about-the-university/term-dates-and-calendars)
so you can find out what the current, next and previous Cambridge term
(or Full term) is on a given date.

There is also a heuristic method, which predicts the correct full term dates for
all Michaelemas terms and most Lent and Easter terms between 2000 and 2030, 
and is one week out when it is wrong. This can be used if you have no internet, 
or want to guess beyond the years currently available, or are ideologically opposed
to web scrapers ;).

Requires `beautifulsoup4` and `requests`.

## Installing as a Python package:

1. Install PyPA `build` module (e.g. `pip install build`)
1. From the `cantabterm/` directory, run `python3 -m build`
1. There should now be files in the `dist` subdirectory, either of which 
   can be installed using `pip install <filename>`

Alternatively just copy `cantabterm/src/cantabterm.py` to your own project.

## Usage

`python3 -m cantabterm` will show heuristic information about today;
`python3 -m cantabterm scrape` will show scraped information about today.

Otherwise `import cantabterm` and obtain the data in some way. Usually the
first time this will come from scraping, e.g. `terms = cantabterm.load_term_data_scrape()`.
In order not to spam the page, you are strongly encouraged to cache the 
data using `terms.save_json("termdata.json")` and next time load it
with `terms = cantabterm.load_term_data_json("termdata.json")`. The 
structure of the JSON file is pretty self explanatory and is intended to
be machine readable such that you can use it with other programming
languages, etc.

Within the Python, `terms` is a `TermDB` object with a few methods, most
of which are documented or self-explanatory. All dates passed to methods
are assumed to be instances of (or comparable to) `datetime.date`. A `Term`
object is a simple container with methods that save you some legwork in
comparing dates.
